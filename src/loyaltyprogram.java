import java.io.IOException;
import java.util.Scanner;

public class loyaltyprogram {
    public static void main(String[] args) throws Exception {

        double v1 = 100;
        double v2 = 200;
        double v3 = 400;
        double v4 = 800;

        String pilih;


        Scanner scan = new Scanner(System.in);

        do{
            System.out.println("Pilih menu");
            System.out.println("1. Redeem Max Point");
            System.out.println("2. Redeem All Point");
            System.out.print("Pilih menu :");
            int menu = scan.nextInt();

            switch (menu) {
                case 1 -> {
                    System.out.print("Reedem Biggest Voucher: ");
                    double point2 = scan.nextInt();
                    System.out.println();
                    System.out.println("Congrats, Here's Your Vouchers!");
                    if (point2 >= v4) { //2000
                        var i = 0;
                        do {
                            point2 = (int) (point2 - v4);
                            i++;
                        } while (point2 >= v4);
                        System.out.println("Rp100.000 x " + i);
                        point2 = point2 % v4;
                    }

                    if (point2 >= v3 && point2 <= v4) {
                        var i = 0;
                        do {
                            point2 = (int) (point2 - v3);
                            i++;
                        } while (point2 >= v3);
                        System.out.println("Rp50.000 x " + i);
                        point2 = point2 % v3;
                    }

                    if (point2 >= v2 && point2 <= v3) {
                        var i = 0;
                        do {
                            point2 = (int) (point2 - v2);
                            i++;
                        } while (point2 >= v2);
                        System.out.println("Rp25.000 x " + i);
                        point2 = point2 % v2;
                    }

                    if (point2 >= v1 && point2 <= v2) {
                        var i = 0;
                        do {
                            point2 = (int) (point2 - v1);
                            i++;
                        } while (point2 >= v1);
                        System.out.println("Rp10.000 x " + i);
                        point2 = point2 % v1;
                    }

                    System.out.println("Sisa : " + point2);
                }
                case 2 -> {
                    System.out.print("Reedem All Point: ");
                    double point = scan.nextInt();
                    if (point < 100) {
                        throw new Exception("Your minimum points must > 100");
                    }
                    System.out.println();
                    System.out.println("Congrats, Here's Your Vouchers!");
                    if (point >= v4) { //2000
                        var i = 0;
                        do {
                            point = (int) (point - v4);
                            i++;
                        } while (point >= v4);
                        System.out.println("Rp100.000 x " + i);
                        point = point % v4;
                    }
                    if (point >= v3) {
                        var x = 0;
                        do {
                            point = (int) (point - v3);
                            x++;
                        } while (point >= v3);
                        System.out.println("Rp50.000 x " + x);
                        point = point % v3;
                    }
                    if (point >= v2) {
                        var j = 0;
                        do {
                            point = (int) (point - v2);
                            j++;
                        } while (point >= v2);
                        System.out.println("Rp25.000 x " + j);
                        point = point % v2;
                    }
                    if (point >= v1) {
                        var y = 0;
                        do {
                            point = (int) (point - v1);
                            y++;
                        } while (point >= v1);
                        System.out.println("Rp10.000 x " + y);
                        point = point % v1;
                    }
                    System.out.println("Sisa : " + point);
                }
            }

            System.out.println();
            System.out.println("Reedem Again ? : Y/N");
            pilih = scan.next();
        }while(pilih.equals("Y"));
    }
}
